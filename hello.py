#!/usr/bin/env python3
'''Simple Hello World Flask Application'''

from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    '''Return the string "Hello, World!"'''
    return "<p>Hello, World!</p>"
