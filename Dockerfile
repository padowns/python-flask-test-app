FROM python:3

# https://github.com/opencontainers/image-spec/blob/main/annotations.md
# LABEL org.opencontainers.image.created
# LABEL org.opencontainers.image.url
# LABEL org.opencontainers.image.source
# LABEL org.opencontainers.image.revision
# LABEL org.opencontainers.image.vendor
# LABEL org.opencontainers.image.documentation
## https://spdx.dev/spdx-specification-21-web-version/
LABEL org.opencontainers.image.licenses="GPL-2.0"
LABEL org.opencontainers.image.title="Hello"
LABEL org.opencontainers.image.version="1.0"
LABEL org.opencontainers.image.description="Simple Hello World Flask App"
LABEL org.opencontainers.image.authors="Peter Downs <padowns@gmail.com>"

#ARG DEBIAN_FRONTEND="noninteractive"
#ARG DPKG_OPTS='-o Dpkg::Options::="--force-confold" -o Dpkg::Options::="--force-confdef"'
ARG DPKG_OPTS=""
ARG EXTRA_PACKAGES="python3-pip python3-dev"

RUN echo "Upgrading Base OS" && \
	apt-get update && \
	apt-get ${DPKG_OPTS} -yq upgrade && \
	apt-get ${DPKG_OPTS} -yq install ${EXTRA_PACKAGES} && \
	rm -rf /var/lib/apt/*

WORKDIR /app

COPY . /app

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r /app/requirements.txt

EXPOSE 5000/tcp

ENV FLASK_APP="hello"
ENV FLASK_ENV="development"
ENTRYPOINT [ "python3" ]
CMD [ "-m", "flask", "run" ] 
